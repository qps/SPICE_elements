Version 4
SymbolType CELL
LINE Normal 16 80 16 96
LINE Normal 8 32 8 80
LINE Normal 24 32 8 32
LINE Normal 16 16 16 32
LINE Normal 0 16 0 24
LINE Normal 0 24 32 84
LINE Normal 32 92 32 84
LINE Normal 24 80 24 32
LINE Normal 8 80 24 80
LINE Normal 20 41 18 38
LINE Normal 20 38 18 41
WINDOW 0 36 40 Left 2
WINDOW 3 37 74 Left 2
SYMATTR Value here_put_subcircuitName
SYMATTR Prefix X
SYMATTR Description Generic varistor symbol for use with a subcircuit that you supply
SYMATTR ModelFile circuit.lib
PIN 16 16 NONE 0
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 16 96 NONE 0
PINATTR PinName B
PINATTR SpiceOrder 2
