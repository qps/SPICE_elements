Version 4
SymbolType CELL
LINE Normal -96 -64 -56 -64
LINE Normal -56 -32 -56 -64
LINE Normal -56 -8 -56 32
LINE Normal -96 32 -56 32
LINE Normal -72 -32 -40 -32
LINE Normal -56 -8 -40 -32
LINE Normal -56 -8 -72 -32
LINE Normal -72 -8 -40 -8
LINE Normal -11 -1 -26 -12
LINE Normal -19 9 -34 -2
LINE Normal -11 -1 -12 -8
LINE Normal -19 9 -20 3
LINE Normal -26 9 -19 9
LINE Normal -18 0 -11 -1
LINE Normal 76 -76 76 -62
LINE Normal 81 -69 76 -76
LINE Normal 86 -76 81 -69
LINE Normal 86 -62 86 -76
RECTANGLE Normal -96 -80 96 48
WINDOW 0 0 -96 Center 2
WINDOW 3 0 64 Center 2
SYMATTR Value here_put_modelName
SYMATTR Prefix D
SYMATTR Description Generic optocoupler LED like symbol for use with a model that you supply.
PIN -96 -64 NONE 0
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -96 32 NONE 0
PINATTR PinName K
PINATTR SpiceOrder 2
