Version 4
SymbolType CELL
LINE Normal 0 32 -4 36
LINE Normal 32 32 36 28
LINE Normal 0 32 32 32
LINE Normal 0 8 32 8
LINE Normal 32 8 16 32
LINE Normal 0 8 16 32
LINE Normal 0 56 32 56
LINE Normal 32 56 16 32
LINE Normal 0 56 16 32
LINE Normal 16 0 16 8
LINE Normal 16 56 16 64
LINE Normal 23 14 20 11
LINE Normal 23 11 20 14
WINDOW 0 24 -2 Left 2
WINDOW 3 24 65 Left 2
SYMATTR Value here_put_subcircuitName
SYMATTR Prefix X
SYMATTR Description Generic Transient Voltage supressor symbol for use with a subcircuit that you supply.
SYMATTR ModelFile circuit.lib
PIN 16 0 NONE 0
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 16 64 NONE 0
PINATTR PinName -
PINATTR SpiceOrder 2
