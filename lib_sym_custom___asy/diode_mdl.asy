Version 4
SymbolType CELL
LINE Normal 0 44 32 44
LINE Normal 0 20 32 20
LINE Normal 32 20 16 44
LINE Normal 0 20 16 44
LINE Normal 16 0 16 20
LINE Normal 16 44 16 64
LINE Normal 8 27 8 23
LINE Normal 10 25 8 23
LINE Normal 12 23 10 25
LINE Normal 12 27 12 23
WINDOW 0 24 0 Left 2
WINDOW 3 24 64 Left 2
SYMATTR Value here_put_modelName
SYMATTR Prefix D
SYMATTR Description Generic Diode symbol for use with a model that you supply.
PIN 16 0 NONE 0
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 16 64 NONE 0
PINATTR PinName K
PINATTR SpiceOrder 2
