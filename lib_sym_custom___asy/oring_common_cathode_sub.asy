Version 4
SymbolType CELL
LINE Normal 29 82 3 82
LINE Normal 3 18 16 44
LINE Normal 3 44 29 44
LINE Normal 29 18 16 44
LINE Normal 29 18 3 18
LINE Normal 16 82 29 108
LINE Normal 16 82 3 108
LINE Normal 3 108 29 108
LINE Normal 16 128 16 0
LINE Normal -16 64 16 64
WINDOW 3 31 66 Left 2
WINDOW 0 45 34 Left 2 
SYMATTR Value here_put_subcircuitName
SYMATTR Prefix X
SYMATTR Description Generic double diode with common cathode symbol for use with a subcircuit that you supply.
SYMATTR ModelFile circuit.lib
PIN 16 0 NONE 8
PINATTR PinName A1
PINATTR SpiceOrder 1
PIN -16 64 NONE 8
PINATTR PinName K
PINATTR SpiceOrder 3
PIN 16 128 NONE 8
PINATTR PinName A2
PINATTR SpiceOrder 2
