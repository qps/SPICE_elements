Version 4
SymbolType CELL
LINE Normal 0 27 -4 31
LINE Normal 32 27 36 23
LINE Normal 0 27 32 27
LINE Normal 0 14 32 14
LINE Normal 32 14 16 27
LINE Normal 0 14 16 27
LINE Normal 16 0 16 14
LINE Normal 16 48 16 64
LINE Normal 32 48 0 48
LINE Normal 16 38 32 48
LINE Normal 0 48 16 38
LINE Normal 32 38 0 38
LINE Normal 37 33 32 38
LINE Normal -4 42 0 38
LINE Normal 16 38 16 27
LINE Normal 12 20 8 16
LINE Normal 12 16 8 20
WINDOW 0 24 0 Left 2
WINDOW 3 25 60 Left 2
SYMATTR Value here_put_subcircuitName
SYMATTR Prefix X
SYMATTR Description Generic Transient Voltage supressor symbol for use with a subcircuit that you supply.
SYMATTR ModelFile circuit.lib
PIN 16 0 NONE 0
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 16 64 NONE 0
PINATTR PinName -
PINATTR SpiceOrder 2
