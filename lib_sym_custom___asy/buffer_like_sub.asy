Version 4
SymbolType CELL
LINE Normal -32 -32 32 0
LINE Normal -32 32 32 0
LINE Normal -32 -32 -32 32
LINE Normal 0 -32 0 -16
LINE Normal 0 32 0 16
LINE Normal 4 -20 12 -20
LINE Normal 8 -24 8 -16
LINE Normal 4 20 12 20
WINDOW 0 16 -32 Left 2
WINDOW 3 16 32 Left 2
SYMATTR Value here_put_subcircuitName
SYMATTR Prefix X
SYMATTR Description Generic Buffer like symbol for use with a subcircuit that you supply.
SYMATTR ModelFile circuit.lib
PIN -32 0 NONE 0
PINATTR PinName in
PINATTR SpiceOrder 1
PIN 32 0 NONE 0
PINATTR PinName out
PINATTR SpiceOrder 2
PIN 0 -32 NONE 0
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN 0 32 NONE 0
PINATTR PinName V-
PINATTR SpiceOrder 4
