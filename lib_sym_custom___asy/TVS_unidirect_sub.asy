Version 4
SymbolType CELL
LINE Normal 0 44 -4 48
LINE Normal 32 44 36 40
LINE Normal 0 44 32 44
LINE Normal 0 20 32 20
LINE Normal 32 20 16 44
LINE Normal 0 20 16 44
LINE Normal 16 0 16 20
LINE Normal 16 44 16 64
LINE Normal 11 26 8 22
LINE Normal 11 22 8 26
WINDOW 0 0 0 Left 2
WINDOW 3 100 0 Left 2
SYMATTR Value here_put_subcircuitName
SYMATTR Prefix X
SYMATTR Description Generic Transient Voltage supressor symbol for use with a subcircuit that you supply.
SYMATTR ModelFile circuit.lib
PIN 16 0 NONE 0
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 16 64 NONE 0
PINATTR PinName -
PINATTR SpiceOrder 2
