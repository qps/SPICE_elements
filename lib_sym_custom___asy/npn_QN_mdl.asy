Version 4
SymbolType CELL
LINE Normal 44 76 36 84
LINE Normal 64 96 44 76
LINE Normal 64 96 36 84
LINE Normal 40 80 16 64
LINE Normal 16 80 16 16
LINE Normal 16 32 64 0
LINE Normal 16 48 0 48
LINE Normal 27 30 27 38
LINE Normal 30 34 27 30
LINE Normal 33 30 30 34
LINE Normal 33 38 33 30
WINDOW 0 56 32 Left 2
WINDOW 3 56 68 Left 2
SYMATTR Value here_put_modelName
SYMATTR Prefix QN
SYMATTR Description Generic Bipolar NPN transistor symbol for use with a model that you supply.
PIN 64 0 NONE 0
PINATTR PinName C
PINATTR SpiceOrder 1
PIN 0 48 NONE 0
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 64 96 NONE 0
PINATTR PinName E
PINATTR SpiceOrder 3
