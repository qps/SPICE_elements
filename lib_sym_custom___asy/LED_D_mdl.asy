Version 4
SymbolType CELL
LINE Normal 0 44 32 44
LINE Normal 0 20 32 20
LINE Normal 32 20 16 44
LINE Normal 0 20 16 44
LINE Normal 16 0 16 20
LINE Normal 16 44 16 64
LINE Normal 72 32 68 40
LINE Normal 72 32 64 32
LINE Normal 72 48 68 56
LINE Normal 72 48 64 48
LINE Normal 7 22 7 26
LINE Normal 8 23 7 22
LINE Normal 9 24 8 23
LINE Normal 11 22 9 24
LINE Normal 11 26 11 22
ARC Normal 40 20 56 36 56 28 40 24
ARC Normal 56 20 72 36 56 28 72 32
ARC Normal 40 36 56 52 56 44 40 40
ARC Normal 56 36 72 52 56 44 72 48
WINDOW 0 24 0 Left 2
WINDOW 3 24 64 Left 2
SYMATTR Value here_put_modelName
SYMATTR Prefix D
SYMATTR Description Generic Light Emitting Diode symbol for use with a model that you supply.
PIN 16 0 NONE 0
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 16 64 NONE 0
PINATTR PinName K
PINATTR SpiceOrder 2
