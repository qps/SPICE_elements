Version 4
SymbolType CELL
LINE Normal -48 32 -32 32
LINE Normal -32 32 -24 36
LINE Normal -48 80 -32 80
LINE Normal -32 80 -24 76
LINE Normal 0 96 0 72
LINE Normal 0 16 0 36
LINE Normal 0 36 20 60
LINE Normal -48 72 -40 72
LINE Normal -44 76 -44 68
LINE Normal -48 40 -40 40
LINE Normal -27 52 -27 61
LINE Normal -24 55 -27 52
LINE Normal -21 52 -24 55
LINE Normal -21 61 -21 52
CIRCLE Normal -32 24 32 88
CIRCLE Normal -4 76 4 68
CIRCLE Normal 16 56 24 64
WINDOW 0 24 16 Left 2
WINDOW 3 24 96 Left 2
SYMATTR Value here_put_modelName
SYMATTR Prefix S
SYMATTR Description Generic Voltage Controlled Switch symbol for use with a model that you supply.
PIN 0 16 NONE 0
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 0 96 NONE 0
PINATTR PinName B
PINATTR SpiceOrder 2
PIN -48 80 NONE 0
PINATTR PinName ctrl+
PINATTR SpiceOrder 3
PIN -48 32 NONE 0
PINATTR PinName ctrl-
PINATTR SpiceOrder 4
