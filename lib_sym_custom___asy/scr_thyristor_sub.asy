Version 4
SymbolType CELL
LINE Normal 0 44 32 44
LINE Normal 0 20 32 20
LINE Normal 32 20 16 44
LINE Normal 0 20 16 44
LINE Normal 16 0 16 20
LINE Normal 16 44 16 64
LINE Normal -12 64 -32 64
LINE Normal -12 64 8 44
LINE Normal 24 25 22 23
LINE Normal 24 23 22 25
WINDOW 0 24 0 Left 2
WINDOW 3 24 72 Left 2
SYMATTR Value here_put_subcircuitName
SYMATTR Prefix X
SYMATTR Description Generic SCR (thyristor) symbol for use with a subcircuit that you supply.
SYMATTR ModelFile circuit.lib
PIN 16 0 NONE 0
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -32 64 NONE 0
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 16 64 NONE 0
PINATTR PinName K
PINATTR SpiceOrder 3
